/// Convenience macro to create a `HashMap` from a predfined set of key - value pairs
///
/// # Example
/// ```
/// let map = hash_map! {
///     "yes" => 0.,
///     "no" => 1.,
///     "pi" => std::f64::consts::PI
/// };
///
/// for (key, val) in map.iter() {
///     println!("Key: {key}, Value: {val}");
/// }
/// ```
#[macro_export]
macro_rules! hash_map {
    ($($key:expr => $val:expr), *) => {
        {
            let mut map = std::collections::HashMap::new();
            $( map.insert($key, $val); )*
            map
        }
    }
}

/// Convenience macro for generating a test module from a set of functions
///
/// It also pre-imports everything from the outer module
#[macro_export]
macro_rules! tests {
    ($($(#[$attr:meta]) * fn $name:ident $params:tt $body:tt) *) => {
        #[cfg(test)]
        mod tests {
            use super::*;

            $(
                $(
                    #[$attr]
                )*
                #[test]
                fn $name$params$body
            )*
        }
    }
}