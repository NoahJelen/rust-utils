#![doc = include_str!("../README.md")]

#[cfg(test)]
mod tests;

pub mod utils;
pub mod logging;
pub mod config;

mod macros;

#[cfg(feature = "linux")]
pub mod linux;

#[cfg(feature = "futures")]
pub mod futures;

pub use rust_utils_macros::*;