//! Utilities for dealing with futures and asynchronous Rust APIs

use std::future::Future;
use tokio::runtime::Runtime;
use futures::executor::block_on;

/// Convenience function to run a future
/// until it returns a value
///
/// This is useful in synchronous contexts where the .await syntax normally can't
/// be used
pub fn exec_future<F: Future>(future: F) -> F::Output {
    let rt = Runtime::new().unwrap();
    let guard = rt.enter();
    let val = block_on(future);
    drop(guard);
    val
}